package com.example.firstgitlabproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class FirstGitlabProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirstGitlabProjectApplication.class, args);
    }

    @GetMapping
    public String defaultGet() {
        return "OK";
    }
}
